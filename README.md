# MFL 10s ADP

Simple NodeJS app for archiving MFL10s ADP via JSON responses from their API. Runs at 9pm (local time) daily and stores JSON responses from the MFL API in the `/data` directory as flat files.

For use with an app that analyses historical ADP data.

## How to start the app

With [Docker](https://get.docker.com):

```bash
git clone https://danabrey@bitbucket.org/danabrey/mfl10sadp.git
cd mfl10sadp
docker-compose up -d
```

Seed the `data` directory with historical ADP data from the MFL API (this will somehow be available here, either in the repo or as a download)

## What does it do?

- On `docker-compose up -d`, Docker containers for the backend API/task runner and the frontend web app will start
- It will also start an Nginx proxy Docker container that routes HTTP traffic to the API (if `/api/*`) or the frontend web app
- You can include historical ADP data by downloading the latest compressed file from this repository's downloads section (**TODO!**) and copying its contents into the `data` directory in the root of the project
- The backend app will once per day run the task to fetch player and ADP data from the MyFantasyLeague API, and store the data in JSON files in the `data` directory
- Once data has been fetched, it will update its own DB (Sqlite at the moment, but could be MariaDB/Postgres) from those newly cached JSON files

## API

The API provides two endpoints for fetching ADP data:

- `/api/adp/all`  returns a JSON response including an array of all available players in MFL10s, with their latest ADP, plus minimum, maximum and the percentage of total drafts in which they've been selected
- `/api/adp/player/:playerID` returns a JSON response of more details information on one player, along with all historical ADP data for that player

## To do

- Add a `/api/adp/players` endpoint, for multiple player IDs to be requested at once. This will be useful for graphs and save multiple requests 
- Make the daily update from the MFL API promise-based, for ease and code clarity
- Switch from Sqlite to MariaDB or Postgres
- Create jobs that query the data daily for trends and top-lists, such as "Top 5 rising players in the past 7 days". Could store it daily in a key/value store like Redis, and add an API endpoint that returns it for the frontend view
- Provide a way to download all historical ADP data to seed a new project

## Notes

Manual daily update:

- Run `node daily-fetch-data-job.js` manually somehow (edit it to do it now)
- Wait for it to fetch new data, and then run `node store-players-data.js` 
- Run `node store-players-adp-data.js`

(this all needs to be async and promise-based)