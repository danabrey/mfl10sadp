import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import { browserHistory } from 'react-router';

import Routes from './routes';

ReactDOM.render(
    <Routes history={browserHistory} />,
    document.getElementById('root')
);