import React, { Component } from 'react';
import { Container, Header } from 'semantic-ui-react';
import { Link } from 'react-router';
import styles from "./css/app.css";

class App extends Component {
  render() {
    return (
      <Container>
        <Header><h1><Link to="/">MFL10s Addict</Link></h1></Header>
          {this.props.children}
      </Container>
    );
  }
}

export default App;
