var format = require("date-format");

Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
};

Date.prototype.oneYearAgo = function() {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() - 365);
    return dat;
};

const yesterday = ( d => new Date(d.setDate(d.getDate()-1)) )( new Date() );

function getDates(startDate, stopDate) {
    var dateArray = [];
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push( format.asString("yyyy-MM-dd", new Date (currentDate)) );
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

const DateRange = getDates(yesterday.oneYearAgo(), yesterday);
export default DateRange;