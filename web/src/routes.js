// src/routes.js
import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';

import App from './App';
import ADPTable from './components/adp-table';
import PlayerDetail from "./components/player-detail";

const Routes = (props) => (
    <Router {...props}>
        <Route path="/" component={App}>
            <IndexRoute component={ADPTable}/>
            <Route path="/player/:id" component={PlayerDetail}/>
        </Route>
        {/*<Route path="*" component={NotFound} />*/}
    </Router>
);

export default Routes;