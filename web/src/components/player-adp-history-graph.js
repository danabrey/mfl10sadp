import React, { Component } from 'react';
import {Line} from 'react-chartjs-2';
import DateRanges from '../config/date-ranges';

class PlayerADPHistoryGraph extends Component {
    constructor(props) {
        super();
        this.state = {
            dataForChart: {}
        }
    }
    componentWillReceiveProps(nextProps) {
        let dates = DateRanges.slice(-Math.abs(nextProps.days));
        let data = dates.map(date => {
            return nextProps.adp.hasOwnProperty(date) ? nextProps.adp[date].average : 241;
        });
        this.setState({
            dataForChart: {
                labels: dates,
                datasets: [
                    {
                        label: "Average ADP",
                        data: data
                    }
                ]
            }
        });
    }
    render() {
        return (
            <Line data={this.state.dataForChart} options={{
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMax: 30,
                            min: 1,
                            reverse: true
                        }
                    }]
                }
            }}/>
        );
    }
}

PlayerADPHistoryGraph.PropTypes = {
    adp: React.PropTypes.array,
    dates: React.PropTypes.object
};

export default PlayerADPHistoryGraph;

