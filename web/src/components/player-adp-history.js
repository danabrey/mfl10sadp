import React, { Component } from 'react';
import PlayerADPHistoryGraph from './player-adp-history-graph';

class PlayerADPHistory extends Component {
    constructor(props) {
        super();
        this.state = {
            days: "14"
        };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        this.setState({
            days: e.target.value
        })
    }
    render() {
        return (
            <div>
                <h3>ADP history for this player</h3>
                <label>Days:
                    <select onChange={this.handleChange} value={this.state.days}>
                        <option value="7">7</option>
                        <option value="14">14</option>
                        <option value="30">30</option>
                        <option value="60">60</option>
                        <option value="90">90</option>
                        <option value="150">150</option>
                        <option value="365">365</option>
                    </select>
                </label>
                <PlayerADPHistoryGraph adp={this.props.adp} days={this.state.days}/>
            </div>
        );
    }
}

PlayerADPHistory.PropTypes = {
    adp: React.PropTypes.array
};

export default PlayerADPHistory;

