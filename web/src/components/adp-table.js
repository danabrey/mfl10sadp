import React, { Component } from 'react';
import 'whatwg-fetch';
import { Table, Thead, Th, Tr, Td } from 'reactable';
import { Link } from 'react-router';

class ADPTable extends Component {
    constructor(props) {
        super();
        this.state = {
            players: []
        }
    }
    componentDidMount() {
        fetch('/api/adp')
            .then(response => {
                return response.json()
            }).then(json => {
            this.setState({
                players: json.data.map((player, index) => {
                    player.rank = index+1;
                    player.link = "/player/" + player.id;
                    return player;
                })
            });
        }).catch(error => {
            console.log('parsing failed', error)
        })
    }
    renderPlayerRows() {
        return this.state.players.map(player => {
            return (
                <Tr key={"player" + player.id}>
                    <Td column="rank" data={player.rank}>{player.rank}</Td>
                    <Td column="name" value={player.name}>
                        <Link to={"/player/" + player.id}>{player.name}</Link>
                    </Td>
                    <Td column="team" data={player.team}>{player.team}</Td>
                    <Td column="position" data={player.position}>{player.position}</Td>
                    <Td column="averageADP" data={player.averageADP}>{player.averageADP}</Td>
                    <Td column="minimumADP" data={player.minimumADP}>{player.minimumADP}</Td>
                    <Td column="maximumADP" data={player.maximumADP}>{player.maximumADP}</Td>
                </Tr>
            );
        })
    }
    render() {
            return (
            <Table className="ui table" itemsPerPage={12} sortable={true} filterable={["name", "team"]}>
                <Thead>
                    <Th column="rank">Rank</Th>
                    <Th column="name">Name</Th>
                    <Th column="team">Team</Th>
                    <Th column="position">Position</Th>
                    <Th column="averageADP">Average ADP</Th>
                    <Th column="minimumADP">Minimum ADP</Th>
                    <Th column="maximumADP">Maximum ADP</Th>
                </Thead>
                {this.renderPlayerRows()}
            </Table>
        );
    }
}

export default ADPTable;

