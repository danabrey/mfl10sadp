import React, { Component } from 'react';
import 'whatwg-fetch';
import PlayerADPHistory from './player-adp-history';

class PlayerDetail extends Component {
    constructor(props) {
        super();
        this.state = {
            player: {
                adp: {}
            }
        }
    }
    componentDidMount() {
        fetch('/api/adp/player/'+this.props.params.id)
            .then(function(response) {
                return response.json()
            }).then(json => {
            this.setState({
                player: json.data
            });
        }).catch(function(ex) {
            console.log('parsing failed', ex)
        })
    }
    render() {
        return (
            <div>
                <h1>{this.state.player.name}</h1>
                <h2>{this.state.player.position}</h2>
                <h3>{this.state.player.team}</h3>
                {/*<PlayerADPHistoryGraph adp={this.state.player.adp} days={7} />*/}
                <PlayerADPHistory adp={this.state.player.adp} />
            </div>
        );
    }
}

export default PlayerDetail;

