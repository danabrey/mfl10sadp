var request = require("request");
var fs = require("fs");
var format = require('date-format');

function fetchData() {

    var currentDate = new Date();
    var currentDateString = format.asString("yyyy-MM-dd", currentDate);

    request("http://www03.myfantasyleague.com/2017/export?TYPE=players&DETAILS=1&JSON=1")
        .pipe(fs.createWriteStream("./data/" + currentDateString + "_players.json"));

    request("http://www03.myfantasyleague.com/2017/export?TYPE=adp&IS_KEEPER=3&IS_MOCK=0&JSON=1")
        .pipe(fs.createWriteStream("./data/" + currentDateString + "_adp-recent.json"));

    request("http://www03.myfantasyleague.com/2017/export?TYPE=adp&IS_KEEPER=3&IS_MOCK=0&TIME=1483246800&JSON=1")
        .pipe(fs.createWriteStream("./data/" + currentDateString + "_adp-all.json"));

    request("http://www03.myfantasyleague.com/2017/export?TYPE=adp&IS_KEEPER=3&IS_MOCK=0&JSON=1&DAYS=1")
        .pipe(fs.createWriteStream("./data/" + currentDateString + "_adp-last1.json"));

    request("http://www03.myfantasyleague.com/2017/export?TYPE=adp&IS_KEEPER=3&IS_MOCK=0&JSON=1&DAYS=7")
        .pipe(fs.createWriteStream("./data/" + currentDateString + "_adp-last7.json"));

    request("http://www03.myfantasyleague.com/2017/export?TYPE=adp&IS_KEEPER=3&IS_MOCK=0&JSON=1&DAYS=14")
        .pipe(fs.createWriteStream("./data/" + currentDateString + "_adp-last14.json"));

    request("http://www03.myfantasyleague.com/2017/export?TYPE=adp&IS_KEEPER=3&IS_MOCK=0&JSON=1&DAYS=30")
        .pipe(fs.createWriteStream("./data/" + currentDateString + "_adp-last30.json"));

    fs.writeFileSync("./data/latest", currentDateString);
}

module.exports = fetchData;