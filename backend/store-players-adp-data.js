var fs = require("fs");
var models = require("./models");
var Player = require("./models").Player;
var PlayerADP = require("./models").PlayerADP;

// Read latest players.json and index them in elasticsearch

fs.readFile("./data/latest", "utf8", (err, data) => {
    let updateDate = data;
    fs.readFile("./data/" + updateDate + "_adp-last7.json", "utf8", (err, data) => {
        try {
            let adp = JSON.parse(data).adp;
            models.sequelize.query("UPDATE Players SET minimumADP = 241, maximumADP = 241, averageADP = 241, selectedInPercent = 0").then(() => {
                adp.player.forEach((result) => {
                    Player.findOne({ where: {mflid: result.id } })
                        .then((player) => {
                            PlayerADP.create({
                                "date": updateDate,
                                "minimum": result.minPick,
                                "maximum": result.maxPick,
                                "average": result.averagePick,
                                "selectedIn": result.draftsSelectedIn,
                                "totalDrafts": adp.totalDrafts
                            }).then(function(adpInstance) {
                                adpInstance.setPlayer(player);
                                player.update({
                                    minimumADP: result.minPick,
                                    maximumADP: result.maxPick,
                                    averageADP: result.averagePick,
                                    selectedInPercent: (result.draftsSelectedIn * 100 / adp.totalDrafts).toFixed(2)
                                });
                            });
                        });
                });
            });
        } catch (e) {
            console.error(e);
        }
    });
});