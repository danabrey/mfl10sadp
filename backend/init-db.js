var models = require("./models");

models.sequelize
    .sync({ force: true })
    .then(function(err) {
        console.log('It worked!');
    }, function (err) {
        console.log('An error occurred while creating the table:', err);
    });