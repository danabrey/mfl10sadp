// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var fs = require("fs");
var Player = require("../models").Player;
var PlayerADP = require("../models").PlayerADP;
var format = require("date-format");

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)

router.get('/', (req, res) => {
    res.json({ status: 'success' });
});

router.get('/adp', function(req, res) {
    Player.findAll({
        where: {
            position: ["QB", "RB", "WR", "TE", "Def", "PK"]
        },
        order: [
            ["averageADP", "ASC"],
            ["name", "ASC"]
        ]
    })
        .then(players => {
            let response = {
                status: "success",
                data: players
            };
            res.json(response);
        })
        .catch(err => {
            res.json({
                status: "error",
                error: err
            });
        });
});

router.get('/adp/player/:id', function(req, res) {
    Player.findOne({
        where: {
            position: ["QB", "RB", "WR", "TE", "Def", "PK"],
            id: req.params.id
        },
        include: [
            PlayerADP
        ]
    })
        .then(player => {
            if (!player) {
                throw "Player not found";
            }
            let playerData = player.get({raw: true});
            let adp = playerData.PlayerADPs.reduce(
                (map, obj) => {
                    map[format.asString("yyyy-MM-dd", obj.date)] = obj;
                    return map;
                }, {}
            );
            playerData.adp = adp;
            res.json({
                status: "success",
                data: playerData
            });
        })
        .catch(err => {
            res.json({
                status: "error",
                error: err
            });
        });
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);