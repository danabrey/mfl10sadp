'use strict';
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Player', {
        "name": DataTypes.STRING,
        "mflid": {
            type: DataTypes.INTEGER,
            unique: true
        },
        "position": DataTypes.STRING,
        "team": DataTypes.STRING,
        "status": DataTypes.STRING,
        "minimumADP": {
            type: DataTypes.INTEGER,
            defaultValue: 241
        },
        "maximumADP": {
            type: DataTypes.INTEGER,
            defaultValue: 241
        },
        "averageADP": {
            type: DataTypes.FLOAT(2),
            defaultValue: 241
        },
        "selectedInPercent": {
            type: DataTypes.FLOAT(2),
            defaultValue: 0
        },
        "draft_year": DataTypes.INTEGER,
        "draft_round": DataTypes.INTEGER,
        "rotoworld_id": DataTypes.INTEGER,
        "stats_id": DataTypes.INTEGER,
        "stats_global_id": DataTypes.INTEGER,
        "espn_id": DataTypes.INTEGER,
        "kffl_id": DataTypes.INTEGER,
        "weight": DataTypes.INTEGER,
        "draft_team": DataTypes.STRING,
        "birthdate": DataTypes.DATE,
        "draft_pick": DataTypes.INTEGER,
        "college": DataTypes.STRING,
        "height": DataTypes.INTEGER,
        "jersey": DataTypes.INTEGER,
        "cbs_id": DataTypes.INTEGER
    }, {
        classMethods: {
            associate: function (models) {
                models.Player.ADP = models.Player.hasMany(models.PlayerADP);
            }
        }
    });
};