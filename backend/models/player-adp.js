'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('PlayerADP', {
      "date": DataTypes.DATE,
      "minimum": DataTypes.INTEGER,
      "maximum": DataTypes.INTEGER,
      "average": DataTypes.FLOAT(2),
      "selectedIn": DataTypes.INTEGER,
      "totalDrafts": DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
          models.PlayerADP.Player = models.PlayerADP.belongsTo(models.Player);
      }
    }
  });
};