var fs = require("fs");
var models = require("./models");
var Player = require("./models").Player;
var format = require('date-format');

// Read latest players.json and index them in elasticsearch

fs.readFile("./data/latest", "utf8", (err, data) => {
    fs.readFile("./data/" + data + "_players.json", "utf8", (err, data) => {
        try {
            let players = JSON.parse(data).players.player;
            // We now have position, id, name, team, and sometimes status ("R" or undefined)
            players.forEach((player) => {
                if (["QB", "RB", "WR", "TE", "Def", "PK"].includes(player.position)) {
                    Player.findOne({ where: { mflid: player.id } })
                        .then(function(obj) {
                            if(obj) { // update
                                return obj.update({
                                    name: player.name,
                                    team: player.team,
                                    position: player.position,
                                    status: player.status,
                                    draft_year: player.draft_year,
                                    draft_round: player.draft_round,
                                    rotoworld_id: player.rotoworld_id,
                                    birthdate: new Date(player.birthdate * 1000),
                                    draft_team: player.draft_team,
                                    stats_id: player.stats_id,
                                    draft_pick: player.draft_pick,
                                    college: player.college,
                                    stats_global_id: player.stats_global_id,
                                    height: player.height,
                                    espn_id: player.espn_id,
                                    jersey: player.jersey,
                                    weight: player.weight,
                                    cbs_id: player.cbs_id
                                });
                            }
                            else { // insert
                                return Player.create({
                                    mflid: player.id,
                                    name: player.name,
                                    team: player.team,
                                    position: player.position,
                                    status: player.status,
                                    draft_year: player.draft_year,
                                    draft_round: player.draft_round,
                                    rotoworld_id: player.rotoworld_id,
                                    birthdate: new Date(player.birthdate * 1000),
                                    draft_team: player.draft_team,
                                    stats_id: player.stats_id,
                                    draft_pick: player.draft_pick,
                                    college: player.college,
                                    stats_global_id: player.stats_global_id,
                                    height: player.height,
                                    espn_id: player.espn_id,
                                    jersey: player.jersey,
                                    weight: player.weight,
                                    cbs_id: player.cbs_id
                                }).then(function(playerInstance) {
                                    // console.log(playerInstance);
                                });
                            }
                        })
                }
            });
        } catch (e) {
            console.error(e);
        }
    });
});